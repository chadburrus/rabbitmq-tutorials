#!/usr/bin/env python
import pika

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

# Prepare the recipient queue -- do this in both places so it doesn't matter which one runs first
channel.queue_declare(queue='hello')

# Print a handy message
print ' [*] Waiting for messages. To exit press CTRL+C'

# The thing to do when a message is received
def callback(ch, method, properties, body):
    print " [x] Received %r" % (body,)

# Set up the consumer
channel.basic_consume(callback,
                      queue='hello',
                      no_ack=True)

# Start consuming
channel.start_consuming()
