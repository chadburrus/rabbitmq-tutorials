#!/usr/bin/env python
import pika

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
                 'localhost'))
channel = connection.channel()

# Prepare the recipient queue -- do this in both places so it doesn't matter which one runs first
channel.queue_declare(queue='hello')

# Send the message
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World!')

# Print a handy message
print " [x] Sent 'Hello World!'"

# Close up shop, flushing buffers and the like
connection.close()
