from Runner import Runner

class Tutorial2Runner(Runner):

	def __init__(self, count):
		super(type(self), self)

		self.count = count

	def kickoff(self):
		count = self.count
		for i in range(1, count):
			message = "Sending message # %s %s" % (i, "." * i)
			command = "python durable_task.py '%s'" % message
			print "Running command \"%s\"" % command
			self._run(command, shell=True)

runner = Tutorial2Runner(30)
runner.kickoff()
