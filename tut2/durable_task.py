#!/usr/bin/env python
import pika
import sys

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
                 'localhost'))
channel = connection.channel()

# Prepare the recipient queue -- do this in both places so it doesn't matter which one runs first
# Durable means that the queue won't be forgotten on RabbitMQ restart
channel.queue_declare(queue='durable_test', durable=True)

# Send the message
message = " ".join(sys.argv[1:]) or "Hello World!"
channel.basic_publish(exchange='',
                      routing_key='durable_test',
                      body=message,
                      # "ensure" the message is persisted to disk just in case
                      properties=pika.BasicProperties(
                        delivery_mode = 2, # make message persistent
                      ))

# Print a handy message
print " [x] Sent %r" % (message, )

# Close up shop, flushing buffers and the like
connection.close()
