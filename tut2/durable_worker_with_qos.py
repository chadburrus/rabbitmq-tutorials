#!/usr/bin/env python
import pika
import time

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

# Prepare the recipient queue -- do this in both places so it doesn't matter which one runs first
# Durable means that the queue won't be forgotten on RabbitMQ restart
channel.queue_declare(queue='durable_test', durable=True)

# Print a handy message
print ' [*] Waiting for messages. To exit press CTRL+C'

# The thing to do when a message is received
def callback(ch, method, properties, body):
    print " [x] Received %r" % (body,)
    time.sleep(body.count("."))
    print " [x] Done"
    ch.basic_ack(delivery_tag = method.delivery_tag)

# Ensure we only pass one job at a time to the worker
channel.basic_qos(prefetch_count=1)

# Set up the consumer
channel.basic_consume(callback,
                      queue='durable_test')

# Start consuming
channel.start_consuming()
