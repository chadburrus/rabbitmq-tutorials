#!/usr/bin/env python
import pika
import sys

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
                 'localhost'))
channel = connection.channel()

# Prepare the recipient queue -- do this in both places so it doesn't matter which one runs first
channel.queue_declare(queue='hello')

# Send the message
message = " ".join(sys.argv[1:]) or "Hello World!"
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=message)

# Print a handy message
print " [x] Sent %r" % (message, )

# Close up shop, flushing buffers and the like
connection.close()
