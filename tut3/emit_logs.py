#!/usr/bin/env python
import pika
import sys

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
                 'localhost'))
channel = connection.channel()

# make an exchange that will pass log information to everyone
channel.exchange_declare(exchange='logs', type='fanout')

# init a random queue so we don't get any old messages, and make sure it's nuked when the consumer dies?  
result = channel.queue_declare(exclusive=True)

# Send the message
message = " ".join(sys.argv[1:]) or "info: Hello World!"
channel.basic_publish(exchange='logs',
                      routing_key='',
                      body=message)

# Print a handy message
print " [x] Sent %s " % message

# Close up shop, flushing buffers and the like
connection.close()
