#!/usr/bin/env python
import pika

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

# make an exchange that will pass log information to everyone
channel.exchange_declare(exchange='logs', type='fanout')

# init a random queue so we don't get any old messages, and make sure it's nuked when the consumer dies?  
result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

# bind the new anonymous queue to the logs exchange so it can receive messages
channel.queue_bind(exchange='logs', queue=queue_name)

# Print a handy message
print ' [*] Waiting for logs. To exit press CTRL+C'

# The thing to do when a message is received
def callback(ch, method, properties, body):
    print " [x] %r" % (body,)

# Set up the consumer
channel.basic_consume(callback,
                      queue=queue_name,
                      no_ack=True)

# Start consuming
channel.start_consuming()
