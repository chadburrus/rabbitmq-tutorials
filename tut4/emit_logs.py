#!/usr/bin/env python
import pika
import sys

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
                 'localhost'))
channel = connection.channel()

# make an exchange that will pass log information to everyone
channel.exchange_declare(exchange='direct_logs', type='direct')

# Send the message
severity = sys.argv[1] if len(sys.argv) > 1 else 'info'
message = ' '.join(sys.argv[2:]) or 'Hello World!'
channel.basic_publish(exchange='direct_logs',
                      routing_key=severity,
                      body=message)

# Print a handy message
print " [x] Sent %s:%s " % (severity, message)

# Close up shop, flushing buffers and the like
connection.close()
