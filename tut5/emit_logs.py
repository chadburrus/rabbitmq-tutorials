#!/usr/bin/env python
import pika
import sys

# make connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(
                 'localhost'))
channel = connection.channel()

# make an exchange that will pass log information to everyone
channel.exchange_declare(exchange='topic_logs', type='topic')

# Send the message
routing_key = sys.argv[1] if len(sys.argv) > 1 else 'anonymous.info'
message = ' '.join(sys.argv[2:]) or 'Hello World!'
channel.basic_publish(exchange='topic_logs',
                      routing_key=routing_key,
                      body=message)

# Print a handy message
print " [x] Sent %s:%s " % (routing_key, message)

# Close up shop, flushing buffers and the like
connection.close()
